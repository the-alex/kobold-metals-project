import React from "react";

interface LegendProps {
  min: number;
  max: number;
}

const Legend: React.FC<LegendProps> = ({ min, max }) => {
  return (
    <div className="absolute z-50 w-full flex bottom-0 right-0 justify-start p-4">
      <div className="bg-white shadow-xl p-4 rounded-md">
        <div className="font-bold">Likelihood</div>
        <div>
          Low ({min}):{" "}
          <span style={{ color: "#6995d3" }}>&#9608;</span>
        </div>
        <div>
          High ({max}):{" "}
          <span style={{ color: "#cc4c02" }}>&#9608;</span>
        </div>
        <div
          className="mt-2 h-1.5 rounded"
          style={{
            background: "linear-gradient(to right, #6995d3, #cc4c02)",
          }}
        ></div>
      </div>
    </div>
  );
};

export default Legend;
