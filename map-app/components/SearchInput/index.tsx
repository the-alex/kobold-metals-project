import React from "react";

interface SearchInputProps {
  value: string;
  onChange: (value: string) => void;
}

const SearchInput: React.FC<SearchInputProps> = ({ value, onChange }) => {
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <div className="absolute z-50 top-0 left-0 w-full flex justify-center p-4">
      <input
        type="text"
        value={value}
        onChange={handleInputChange}
        className="bg-white rounded-md w-full md:w-1/2 px-4 py-2 shadow-md focus:outline-none focus:ring-2 focus:ring-blue-400"
        placeholder="Search for a layer..."
      />
    </div>
  );
};

export default SearchInput;
