import { useState, useEffect, useRef } from 'react';

/**
 * A hook to debounce a value
 */
export const useDebounce = <T>(value: T, delay = 300) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);

      // Cancel the timeout if value changes (also on delay change or unmount)
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-run if value or delay changes
  );

  return debouncedValue;
};

type Timeout = ReturnType<typeof setTimeout>;

/**
 * Hook to debounce a function within a react component render
 * @param callback function to debounce
 * @param delay delay before function is executed
 * @returns function instance to call with a cancel method added
 * @example
 * const debouncedLog = useDebouncedCallback((s: string) => console.log(s), 1000)
 * debouncedLog('one')
 * debouncedLog.cancel()
 */
export const useDebounceCallback = <T>(callback: (...args: T[]) => void, delay = 300) => {
  const timeoutRef = useRef<Timeout>();

  useEffect(() => {
    // we only want to clear the timeout on the unmounting of this component so
    // it's okay that this value can change underneath us
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    return () => timeoutRef.current && clearTimeout(timeoutRef.current);
  }, []);

  const debouncedCallback = (...args: T[]) => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    timeoutRef.current = setTimeout(() => {
      callback(...args);
    }, delay);
  };

  debouncedCallback.cancel = () => timeoutRef.current && clearTimeout(timeoutRef.current);

  return debouncedCallback;
};


// TODO Take the selected layer, return source and layer configuration.
export function fetchLayer(layerName: string) {
  return {
    sourceOpts: {},
    layerOpts: {},
  };
}

// Fetch a geojson file from the server
export async function fetchGeoJson(url: string): Promise<GeoJSON.FeatureCollection> {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

// TODO Take parameters for layer name and return the geojson file
export function buildGeoJsonFilename({
  rateCategory,
  isWinter,
  isWeekday,
  hour,
}: {
  rateCategory: string;
  isWinter: boolean;
  isWeekday: boolean;
  hour: number;
}): string {
  const geoJsonString = `${encodeURIComponent(
    rateCategory
  )}.${isWinter}.${isWeekday}.${hour}.geojson`;
  console.log(geoJsonString);
  return geoJsonString;
}