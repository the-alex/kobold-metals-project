import React, { useRef, useEffect, useState } from "react";
import { fetchGeoJson } from "../utils";
import maplibregl from "maplibre-gl";

interface mapProps {
  selectedLayer: string;
  lng: number;
  lat: number;
  zoom: number;
  API_KEY: string;
}

export default function Map({
  selectedLayer,
  lng,
  lat,
  zoom,
  API_KEY,
}: mapProps) {
  const mapContainer = useRef(null);
  const map = useRef<maplibregl.Map | null>(null);

  const [hasLoaded, setHasLoaded] = useState(false);

  useEffect(() => {
    // Only initialize the map once
    if (map.current) return;
    map.current = new maplibregl.Map({
      container: mapContainer.current!,
      style: `https://api.maptiler.com/maps/dataviz/style.json?key=${API_KEY}`,
      center: [lng, lat],
      zoom: zoom,
    });

    map.current.on("load", () => {
      setHasLoaded(true);
      addLayer(selectedLayer);
    });
  });

  const addLayer = (selectedLayer: string) => {
    if (!map.current) return;

    // Fetch the geojson file from the server
    if (selectedLayer) {
      // Encode the base url for the geojson file
      const url = `/api/layers/${selectedLayer}`;

      fetchGeoJson(url)
        .then((data) => {
          // Compute the min, median, and max of the voll values
          map.current!.addSource(selectedLayer, {
            type: "geojson",
            data: data,
          });

          map.current!.addLayer({
            id: "likelihood",
            type: "circle",
            source: selectedLayer,
            paint: {
              // "circle-radius": 2.5,
              "circle-radius": [
                "interpolate",
                ["linear"],
                // Increase the radius as zoom increases
                ["zoom"],
                0,
                2.5,
                7.5,
                5,
                15,
                7.5,
                20,
                10,
              ],
              "circle-color": [
                "interpolate",
                ["linear"],
                ["get", "likelihood"],
                0.0,
                "#6995d3", // Lighter Blue
                1.0,
                "#cc4c02", // Darker Orange
              ],
              // Decrease opactiy as zoom increases
              "circle-opacity": [
                "interpolate",
                ["linear"],
                ["zoom"],
                0,
                0.75,
                20,
                0.1,
              ],
            },
          });
          // Make layer visible
          map.current!.setLayoutProperty("likelihood", "visibility", "visible");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <div className="relative flex grow">
      <div ref={mapContainer} className="flex grow" />
    </div>
  );
}
