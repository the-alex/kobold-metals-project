import { useState, useEffect, useCallback } from "react";
import Head from "next/head";
import Map from "../components/Map";
// import Sidebar from "../components/Sidebar";
import SelectInput from "../components/SelectInput";
import Legend from "../components/Legend";
import { buildGeoJsonFilename, useDebounceCallback } from "../components/utils";

const formSelectionToFileParameters = (formSelections) => {
  return {
    rateCategory: formSelections.rateCategory,
    hour: formSelections.hour,
    isWinter: formSelections.season === "winter",
    isWeekday: formSelections.weekendOrWeekday === "weekday",
  };
};

// Map options
const [LNG, LAT] = [-122.82, 51.142];
const ZOOM = 6;
const API_KEY = "ndNEoZnG9Ng7i1j1tDQk";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Hello, KoBold!</title>
        <meta
          name="description"
          content="Map layout for KoBold Metals take home"
        />
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://unpkg.com/maplibre-gl@2.4.0/dist/maplibre-gl.css"
          rel="stylesheet"
        />
      </Head>
      <div className="h-screen w-screen flex">
        <div className="relative flex flex-col w-full">
          <Map
            selectedLayer={"likelihood_grid.geojson"}
            lat={LAT}
            lng={LNG}
            zoom={ZOOM}
            API_KEY={API_KEY}
          />
          <Legend min={0.0} max={1.0} />
        </div>
      </div>
    </div>
  );
}
