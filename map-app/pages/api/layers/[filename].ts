import { NextApiRequest, NextApiResponse } from "next";
import fs from "fs";
import path from "path";
import { FeatureCollection } from "geojson";

const readGeoJsonFile = (filePath: string): string =>
  fs.readFileSync(filePath, "utf-8");

const fileExists = (filePath: string): boolean => fs.existsSync(filePath);

const isGeoJsonFile = (fileName: string): boolean =>
  fileName.endsWith(".geojson");

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  try {
    const fileName = Array.isArray(req.query.filename)
      ? req.query.filename[0]
      : req.query.filename;

    if (!fileName) {
      res.status(400).json({ error: "Missing filename" });
      return;
    }

    if (!isGeoJsonFile(fileName)) {
      res
        .status(400)
        .json({ error: "Invalid file format. Must be a .geojson file." });
      return;
    }

    const filePath = path.join(
      process.cwd(),
      "public",
      "data",
      "layers",
      fileName
    );

    if (!fileExists(filePath)) {
      res.status(404).json({ error: "GeoJSON file not found." });
      return;
    }

    const fileContent = readGeoJsonFile(filePath);
    const geoJson: FeatureCollection = JSON.parse(fileContent);

    res.status(200).json(geoJson);
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred while processing the request." });
  }
}
