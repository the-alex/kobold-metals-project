"""
Utilities for the simple command line tool.

Sections include:
- Aspects (logging, command line arguments, etc.)
- Data (loading, cleaning)
- Distance function
- Mapping
"""

import argparse
import logging
import structlog
import sys

from pathlib import Path

import geopandas as gpd
import numpy as np
from shapely.geometry import Point
from shapely.ops import nearest_points

import matplotlib.pyplot as plt
import contextily as cx


def parse_args():
    """Parse command line arguments for this application."""
    parser = argparse.ArgumentParser(description="KoBold Map Tool")
    parser.add_argument(
        "-d", "--data", type=str, help="Path to the data directory", required=True
    )
    parser.add_argument(
        "-o", "--output", type=str, help="Path to the output directory", required=True
    )
    parser.add_argument(
        "--debug",
        help="Enable debugging mode, increasing log level.",
        action="store_true",
    )
    parser.add_argument(
        "-r",
        "--rocks",
        help="List of rocks to include in the map. Defaults to ultramafic, serpentinite, and granodioritic.",
        nargs="+",
        default=["ultramafic", "serpentinite", "granodioritic"],
    )
    parser.add_argument(
        "-f",
        "--falloff",
        type=float,
        help="Float value for falloff paramter (distance in meters) in 'likelihood' function.",
        required=True,
    )
    parser.add_argument(
        "--resolution",
        type=int,
        help="Integer that controls the number of equally spaced points in the grid.",
        default=150,
    )
    parser.add_argument(
        "--geojson",
        help="Write likelihood grid to a geojson file.",
        action="store_true",
    )
    return parser.parse_args()


def setup_logger(debug: bool = False):
    """Setup my preferred logging configuration. Includes extras for verbose
    stack traces, colored output, and module names with line numbers."""

    logging.basicConfig(
        format="%(message)s",
        stream=sys.stdout,
        level=logging.DEBUG if debug else logging.INFO,
    )

    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.dev.ConsoleRenderer(colors=True),
        ],
        context_class=dict,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
    )

    logger = structlog.get_logger()

    return logger


def load_data(filepath: Path):
    """Load the bedrock shapefile."""
    gdf = gpd.read_file(filepath)

    # Select subset of columns
    gdf = (
        gdf[["rock_type", "geometry"]]
        .rename(columns={"rock_type": "rock_type_raw"})
        .assign(rock_type=lambda df: df.rock_type_raw.str.lower())
    )

    # NOTE Is this necessary?
    # Combine geometries by rock type
    gdf = gdf.dissolve(by="rock_type", as_index=False)

    return gdf


def geo_plot(gdf, column=None, crs=None, with_basemap=True, **kwargs):
    """Simple wrapper around geopandas plot so we get a basemap and a decent size."""

    ax = gdf.plot(figsize=(12, 12), column=column, **kwargs)

    if with_basemap:
        cx.add_basemap(
            ax, source=cx.providers.CartoDB.Positron, crs=gdf.crs.to_string()
        )

    if ax:
        ax.set_axis_off()

    return ax


def compute_grid(gdf, resolution):
    # NOTE bbox = (minx, miny, maxx, maxy), or (lng, lat, lng, lat)
    bbox = gdf.total_bounds
    lng_start, lat_start, lng_end, lat_end = bbox

    # Compute the step size
    lat_steps = ((lat_end - lat_start) / resolution) + 1
    lng_steps = ((lng_end - lng_start) / resolution) + 1

    lat_values = np.arange(lat_start, lat_end, lat_steps)
    lng_values = np.arange(lng_start, lng_end, lng_steps)

    # Point is (x, y), so (lng, lat)
    points = [Point(lng, lat) for lat in lat_values for lng in lng_values]

    point_grid = gpd.GeoDataFrame({"geometry": points}, crs=gdf.crs)

    return point_grid


def modified_guassian(x, sigma, k=2.0):
    """
    Guassian used to compute likelihood of a point containing cobalt as a
    function of distance from related deposits. The distribution is centered at
    mu = 0.0, standard deviation sigma determines the width of the distribution,
    and the constant k influences the steepness of the curve.

    NOTE When k != 2.0, is not technically a standard guassian, because we're
    changing the constant in the exponent without dividing by a normalization
    constant.
    """
    # TODO Normalize make a proper probability distribution.
    # C = 1 / (np.sqrt(0.5) * sigma * np.sqrt(np.pi))
    # return C * np.exp(-x ** 2 / (k * sigma ** 2))
    return np.exp(-(x**2) / (k * sigma**2))


def likelihood(distance, falloff):
    """Compute the likelihood of a point given a distance and falloff point."""
    return modified_guassian(distance, falloff, 2.0)


def distance_to_target(point, target_geometry):
    """Compute the distance to the nearest target point."""
    nearest = nearest_points(point, target_geometry)[1]
    return point.distance(nearest)


def compute_likelihood(point, target_geometry, falloff):
    """Compute the likelihood of a point given a target geometry and falloff point."""
    distance = distance_to_target(point, target_geometry)
    return likelihood(distance, falloff)


def generate_heatmap(bedrock_data, likelihood_grid, output_dir):
    ax = geo_plot(bedrock_data, column="target", alpha=0.5)
    geo_plot(
        likelihood_grid,
        ax=ax,
        column="likelihood",
        legend=True,
        markersize=0.5,
    )

    # Save figure
    output_dir.mkdir(parents=True, exist_ok=True)
    output_path = output_dir / "heatmap.png"
    plt.savefig(output_path)

    return output_path
