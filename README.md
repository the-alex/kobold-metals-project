KoBold Take Home
================

## Tasks

- [X] Write command line parser
- [X] Load and clean data
- [X] Implement distance function
- [X] Compute point grid
- [X] Generate map
- [X] Deploy geojson to [link](https://kobold-map.fly.dev)

## Getting Started

This command line tool implemented as a main.py script reads a `BedrockP.shp` shapefile in a provided data directory, and generates a heatmap of potential mineral deposits in an output directory. The likelihood function is a simple modified guassian distribution, with the "falloff" parameter (standard deviation, sigma) as a required argument.

The python environment is defined by `base.env.yml`. I recommend using mamba or conda to install. `[conda|mamba] env create --prefix ./venv --file base.env.yml`

An example invocation might look like this: `./main.py --data materials/data --output output -f 7500`

```
❯ ./main.py --help
usage: main.py [-h] -d DATA -o OUTPUT [--debug] [-r ROCKS [ROCKS ...]] -f FALLOFF [--resolution RESOLUTION] [--geojson]

KoBold Map Tool

optional arguments:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  Path to the data directory
  -o OUTPUT, --output OUTPUT
                        Path to the output directory
  --debug               Enable debugging mode, increasing log level.
  -r ROCKS [ROCKS ...], --rocks ROCKS [ROCKS ...]
                        List of rocks to include in the map. Defaults to ultramafic, serpentinite, and granodioritic.
  -f FALLOFF, --falloff FALLOFF
                        Float value for falloff paramter (distance in meters) in 'likelihood' function.
  --resolution RESOLUTION
                        Integer that controls the number of equally spaced points in the grid.
  --geojson             Write likelihood grid to a geojson file.

```

## Outputs

![Default paramters](example-heatmap.png "Heatmap")

## Map application

There is a map application that loads the output geojson as well, and deployed to fly.io. The layer that has been built and deployed reflects the default paramters.

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
