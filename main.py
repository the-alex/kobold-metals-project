#!venv/bin/python3

from pathlib import Path
import matplotlib.pyplot as plt
import utils as u
import sys


def main(
    *,
    rock_list: list[str],
    falloff: float,
    resolution: float = 150,
    geojson: bool = False,
    data_dir: Path,
    output_dir: Path,
    # TODO Use word stems to handle "-ite" and "-ic" suffixes
    # compute_stems: bool,
):
    # Load data
    log.info("Loading data ...")
    gdf = u.load_data(data_dir / "BedrockP.shp")

    # Assign target column based on rock list
    gdf = gdf.assign(target=lambda df: df.rock_type.str.contains("|".join(rock_list)))
    target_geometry = gdf[gdf.target].unary_union

    # Compute distance matrix
    log.info("Computing Likelihood ...")
    point_grid = u.compute_grid(gdf, resolution)

    likelihood_grid = point_grid.assign(
        likelihood=point_grid.geometry.apply(
            lambda point: u.compute_likelihood(point, target_geometry, falloff)
        )
    )

    log.info("Generating Heatmap ...")
    heatmap_filepath = u.generate_heatmap(gdf, likelihood_grid, output_dir)
    log.info("Saved heatmap", path=heatmap_filepath)

    if geojson:
        geojson_filepath = output_dir / "likelihood_grid.geojson"
        likelihood_grid.to_crs(epsg=4326).to_file(geojson_filepath, driver="GeoJSON")
        log.info("Saved geojson", path=geojson_filepath)


if __name__ == "__main__":
    args = u.parse_args()
    log = u.setup_logger(debug=args.debug)
    log.info("Starting application", args=args)

    rock_list = args.rocks
    falloff = args.falloff
    data_dir = Path(args.data).resolve()
    output_dir = Path(args.output).resolve()

    try:
        main(
            rock_list=rock_list,
            falloff=falloff,
            geojson=args.geojson,
            data_dir=data_dir,
            output_dir=output_dir,
        )
        log.info("Done!")
        sys.exit(0)
    except Exception as e:
        log.exception("exception", exc_info=e)
        sys.exit(1)
